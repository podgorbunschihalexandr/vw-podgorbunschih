### Project details
I used PHP 7.3.9 and Symfony flex(4.3) as a micro framework.
For dependencies you will need composer.

### Architecture details:
I split the task into some smaller parts and this is what i analyzed:

1. We should have a controller for our endpoints(ping and reverse) that is receiving an HTTP 
Request and is returning an HTTP Response

2. Controller is giving the responsibility to a Builder class to transform the request to a response

3. The builder class should take care of building the response, for that he should follow 
process
```   
    1. Normalize request xml string to an SimpleXmlObject
    2. Deserialize the xml request to an Object in order to be able to use it in our application
    3. Transform the request object to a response object
    4. Serialize the response to an xml string
    5. Return an HTTP response

    request *1-> xml *2-> object_request *3-> object_response *4-> xml *5-> response
```

- We should parse the request. 
Theoretical we can have different request types (xml,json,text) 
that's why I created an interface to relay on abstraction.
We should have a parser class who will be responsible for normalization of the data. 
In our case we have an Xml parser.
```
\App\Parser\XmlParser
```
- We should validate the request again xsd schema. And again as we can different 
validators for different types (xml, json, text), we should have an abstraction in order
to easy add new validators. In our case we have just one
```
\App\Validator\XmlValidator
``` 
- Because in our application we don't really want to use SimpleXmlElements or DomDocument,
its better to have a good structured objects for that, that will have a well defined structure.
All models, are described in the folder
```
src/Model
```
- In order to transform an xml to an objects, usually you want to use the 
serialization mechanism for that. In order to not use many 3rd party libraries i created a sample
for Serializer. I think in production its better to use a well known tool for that (JMS Serializer).
That will convert the xml to object.
The serialization process is in folder
```
src/Serializer
```
- After we received an request model, we cam use it in our application logic.
- After we should transform the request model to an response model. For that we have a class that is 
responsible for that. 
```
\App\Builder\ResponseBuilder
```
- After we received an response model, we should serialize it to an XML. Again using the Serialization mechanism
- And the last step would be to create an HTTP Response with the XML generated.

#### Folder structure:
- data/samples - examples
- data/xsds - xsd schemas
- src/Builder (Classes responsible for creating of over objects)
- src/Controller (Place where we are declaring our endpoints)
- src/Model (A well structured representation of Models, Request, Responses, Body Header, Errors)
- src/Parser (Place where we can declare our parser for different formats)
- src/Serializer (Place where we can declare our serializer for different formats. Ideally i would recommend a 3rd party lib)
- src/Validator (Place where we can declare our validator for different formats) 

#### Installation
```
- git clone https://podgorbunschihalexandr@bitbucket.org/podgorbunschihalexandr/vw-podgorbunschih.git
- composer install
```
#### Running the project
```
php -S localhost:8000
```

#### Testing
You can use curl for that.
```
curl -X POST -d @data/samples/reverse_request.xml localhost:8000/public/index.php/api/reverse

```

or with debugger enabled

```
curl -X POST -d @data/samples/reverse_request.xml localhost:8000/public/index.php/api/reverse --cookie "XDEBUG_SESSION=PHPSTORM"
``` 

#### Test cases
Revers request:
```
curl -X POST -d @data/samples/reverse_request.xml localhost:8000/public/index.php/api/reverse
```
Revers response
```
<?xml version="1.0" encoding="UTF-8"?>
<reverse_response>
   <header>
      <type>reverse_response</type>
      <sender>DEMO</sender>
      <recipient>VOICEWORKS</recipient>
      <reference>reverse_request_12345</reference>
      <timestamp>2019-10-02T14:25:52.081408+00:00</timestamp>
   </header>
   <body>
      <string>Hello!</string>
      <reverse>!olleH</reverse>
   </body>
</reverse_response>
```

Ping request:
```
curl -X POST -d @data/samples/ping_request.xml localhost:8000/public/index.php/api/ping
```
Ping response
```
<?xml version="1.0" encoding="UTF-8"?>
<ping_response>
   <header>
      <type>ping_response</type>
      <sender>DEMO</sender>
      <recipient>VOICEWORKS</recipient>
      <reference>ping_request_12345</reference>
      <timestamp>2019-10-02T14:27:18.933580+00:00</timestamp>
   </header>
   <body>
      <echo>Hello!</echo>
   </body>
</ping_response>
```

Wrong request (wrong root tag)
```
curl -X POST -d '<?xml version="1.0" encoding="UTF-8"?><reverse_request2><header><type>reverse_request</type><sender>VOICEWORKS</sender><recipient>DEMO</recipient><reference>reverse_request_12345</reference><timestamp>2013-12-19T16:45:10.950+01:00</timestamp></header><body><string>Hello!</string></body></reverse_request2>' localhost:8000/public/index.php/api/ping
```

Nack response:
```
<?xml version="1.0" encoding="UTF-8"?>
<nack>
   <header>
      <type>nack</type>
      <sender>DEMO</sender>
      <recipient>VOICEWORKS</recipient>
      <reference>reverse_request_12345</reference>
      <timestamp>2019-10-02T14:30:49.160625+00:00</timestamp>
   </header>
   <body>
      <error>
         <code>400</code>
         <message>Request is invalid</message>
      </error>
   </body>
</nack>
```


#### TODO
- Improve the building mechanism, as right now we have an if/else statement there for creating objects
- Improve serialization mechanism
- Improve exception handling

#### Notes
- I didn't wrote unit test, as I was out of time. If you are interested in that i think this can be discussed.
- Again from time trouble, some architectural choices and development choices are not ideal. I am open to discuss them
- From time troubles, all the exception are not catch and treated properly  


