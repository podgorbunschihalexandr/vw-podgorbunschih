<?php

namespace App\Serializer;

use App\Model\Header;
use App\Model\Message;
use App\Model\Request\Body\PingRequestBody;
use App\Model\Request\Body\ReverseRequestBody;
use App\Model\Request\PingRequest;
use App\Model\Request\ReverseRequest;
use App\Model\Response\NackResponse;
use App\Model\Response\PingResponse;
use App\Model\Response\ReverseResponse;

/**
 * In order to work with objects in our application we should use the serialization mechanism.
 * To serialize different formats(xml,json) to objects.
 * Community provide a variety of serialization libraries to automate this process.
 * In order to no complicate i will use a simple implementation.
 * This approach i would not recommend to use in real project.
 */
class XmlSerializer implements SerializerInterface
{
    /**
     * If we would have a Serializer(like JMS os Symfony serialization), this could go to a map in config files
     * @var array
     */
    private $messagesType = [
        'ping_request' => PingRequest::class,
        'ping_response' => PingResponse::class,
        'reverse_request' => ReverseRequest::class,
        'reverse_response' => ReverseResponse::class,
        'nack' => NackResponse::class
    ];

    /**
     * @param $message
     * @param string $format
     * @param array $context
     * @return \SimpleXMLElement
     * @throws \Exception
     *
     * If we would have a Serializer(like JMS os Symfony serialization), this could go to a map in config files
     * In that case we would get rid of if statements.
     * This is breaking at the moment the Open Close Principle, as to add a new response we will need to change this class
     */
    public function serialize($message, $format = 'xml', array $context = [])
    {
        if (!$message instanceof Message) {
            throw new \RuntimeException('Wrong data type');
        }

        $responseXML = new \SimpleXMLElement(sprintf(
            '<?xml version="1.0" encoding="UTF-8"?>%s',
            $this->getNodeTag($message->getHeader()->getType())
        ));

        $header = $responseXML->addChild('header');

        $header->addChild('type', $message->getHeader()->getType());
        $header->addChild('sender', $message->getHeader()->getSender());
        $header->addChild('recipient', $message->getHeader()->getRecipient());
        $header->addChild('reference', $message->getHeader()->getReference());
        $header->addChild('timestamp', (string)(new \DateTime())->format('Y-m-d\TH:i:s.uP'));

        $body = $responseXML->addChild('body');

        if ($message instanceof PingResponse) {
            $body->addChild('echo', $message->getBody()->getEcho());

            return $responseXML;
        }

        if ($message instanceof ReverseResponse) {
            $body->addChild('string', $message->getBody()->getString());
            $body->addChild('reverse', $message->getBody()->getReverse());

            return $responseXML;
        }

        if ($message instanceof NackResponse) {
            $error = $body->addChild('error');
            $error->addChild('code', $message->getBody()->getError()->getCode());
            $error->addChild('message', $message->getBody()->getError()->getMessage());

            return $responseXML;
        }
    }

    /**
     * @param $requestXml
     * @param string $format
     * @return PingRequest|ReverseRequest
     *
     * If we would have a Serializer(like JMS os Symfony serialization), this could go to a map in config files
     * In that case we would get rid of if statements.
     * This is breaking at the moment the Open Close Principle, as to add a new request we will need to change this class
     */
    public function deserialize($requestXml, $format = 'xml')
    {
        if (!$requestXml instanceof \SimpleXMLElement) {
            throw new \RuntimeException('Wrong data type');
        }

        $messageClass = $this->getMessageClass($this->getType($requestXml));

        $header = $this->createHeaderFromXml($requestXml);

        if ($messageClass === PingRequest::class) {
            $body = new PingRequestBody($requestXml->body->echo);
            return new PingRequest($header, $body);
        }

        if ($messageClass === ReverseRequest::class) {
            $body = new ReverseRequestBody($requestXml->body->string);
            return new ReverseRequest($header, $body);
        }
    }

    /**
     * @param \SimpleXMLElement $element
     * @return Header
     */
    private function createHeaderFromXml(\SimpleXMLElement $element)
    {
        $header = new Header(
            $element->header->type,
            $element->header->sender,
            $element->header->recipient,
            $element->header->reference,
            $element->header->timestamp
        );

        return $header;
    }

    /**
     * @param $type
     * @return string
     */
    private function getNodeTag($type): string
    {
        return sprintf('<%s></%s>', $type, $type);
    }

    /**
     * @param $type
     * @return mixed
     */
    private function getMessageClass($type)
    {
        if (isset($this->messagesType[$type])) {
            return $this->messagesType[$type];
        }

        throw new \InvalidArgumentException('Wrong type');
    }

    /**
     * @param \SimpleXMLElement $requestXml
     * @return string
     */
    private function getType(\SimpleXMLElement $requestXml)
    {
        return (string)$requestXml->header->type;
    }
}
