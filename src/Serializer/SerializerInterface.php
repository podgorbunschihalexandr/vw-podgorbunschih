<?php

namespace App\Serializer;

interface SerializerInterface
{
    public function serialize($data, $format);

    public function deserialize($data, $format);
}
