<?php

namespace App\Controller;

use App\Builder\MessageBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(path="/api", name="api_")
 */
class ApiController extends AbstractController
{
    /**
     * @Route(path="/request", name="request", methods={"POST"})
     * @param Request $request
     * @param MessageBuilderInterface $messageBuilder
     * @return Response
     */
    public function request(Request $request, MessageBuilderInterface $messageBuilder): Response
    {
        return $messageBuilder->build($request);
    }

    /**
     * @Route(path="/ping", name="ping", methods={"POST"})
     * @param Request $request
     * @param MessageBuilderInterface $messageBuilder
     * @return Response
     */
    public function ping(Request $request, MessageBuilderInterface $messageBuilder): Response
    {
        return $messageBuilder->build($request);
    }

    /**
     * @Route(path="/reverse", name="reverse", methods={"POST"})
     * @param Request $request
     * @param MessageBuilderInterface $messageBuilder
     * @return Response
     */
    public function reverse(Request $request, MessageBuilderInterface $messageBuilder): Response
    {
        return $messageBuilder->build($request);
    }
}
