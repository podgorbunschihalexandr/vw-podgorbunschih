<?php

namespace App\Builder;

use App\Parser\XmlParser;
use App\Serializer\XmlSerializer;
use App\Validator\XmlValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class XmlMessageBuilder implements MessageBuilderInterface
{
    /**
     * @var XmlParser
     */
    private $xmlParser;

    /**
     * @var XmlValidator
     */
    private $xmlValidator;

    /**
     * @var XmlSerializer
     */
    private $xmlSerializer;

    /**
     * @var ResponseBuilder
     */
    private $responseBuilder;

    /**
     * XMLMessageBuilder constructor.
     * @param XmlParser $xmlParser
     * @param XmlValidator $xmlValidator
     * @param XmlSerializer $xmlSerializer
     * @param ResponseBuilder $responseBuilder
     */
    public function __construct(
        XmlParser $xmlParser,
        XmlValidator $xmlValidator,
        XmlSerializer $xmlSerializer,
        ResponseBuilder $responseBuilder
    )
    {
        $this->xmlParser = $xmlParser;
        $this->xmlValidator = $xmlValidator;
        $this->xmlSerializer = $xmlSerializer;
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function build(Request $request): Response
    {
        $requestXML = $this->xmlParser->parse($request->getContent());

        $isValid = $this->xmlValidator->isValid($requestXML);

        $requestModel = $this->xmlSerializer->deserialize($requestXML);

        if ($isValid) {
            $responseModel = $this->responseBuilder->buildResponse($requestModel);
        } else {
            $responseModel = $this->responseBuilder->buildErrorResponse($requestModel, 400, 'Request is invalid');
        }

        $responseXML = $this->xmlSerializer->serialize($responseModel);

        $response = new Response($responseXML->asXML());
        $response->headers->set('Content-Type', 'text/xml');
        return $response;
    }
}
