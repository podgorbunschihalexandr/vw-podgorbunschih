<?php

namespace App\Builder;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface MessageBuilderInterface
{
    public function build(Request $request): Response;
}
