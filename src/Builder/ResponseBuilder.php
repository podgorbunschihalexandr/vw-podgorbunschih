<?php

namespace App\Builder;

use App\Model\Error;
use App\Model\Header;
use App\Model\Message;
use App\Model\Request\PingRequest;
use App\Model\Request\ReverseRequest;
use App\Model\Response\Body\NackResponseBody;
use App\Model\Response\Body\PingResponseBody;
use App\Model\Response\Body\ReverseResponseBody;
use App\Model\Response\NackResponse;
use App\Model\Response\PingResponse;
use App\Model\Response\ReverseResponse;

class ResponseBuilder
{
    /**
     * @param Message $requestMessage
     * @return Message
     * @throws \Exception
     */
    public function buildResponse(Message $requestMessage): Message
    {
        $header = $this->createHeaderResponse($requestMessage->getHeader());

        if ($requestMessage instanceof PingRequest) {
            $body = new PingResponseBody($requestMessage->getBody()->getEcho());
            return new PingResponse($header, $body);
        }

        if ($requestMessage instanceof ReverseRequest) {
            $body = new ReverseResponseBody($requestMessage->getBody()->getString());
            return new ReverseResponse($header, $body);
        }

        return $this->buildErrorResponse($requestMessage, 400, 'Unknown request');
    }

    /**
     * @param Message $requestMessage
     * @param int $code
     * @param string $message
     * @return Message
     * @throws \Exception
     */
    public function buildErrorResponse(Message $requestMessage, int $code, string $message): Message
    {
        return new NackResponse(
            $this->createHeaderNack($requestMessage->getHeader()),
            new NackResponseBody(
                new Error($code, $message)
            )
        );
    }

    /**
     * @param Header $requestHeader
     * @return Header
     * @throws \Exception
     */
    private function createHeaderNack(Header $requestHeader): Header
    {
        return new Header(
            'nack',
            $requestHeader->getRecipient(),
            $requestHeader->getSender(),
            $requestHeader->getReference(),
            (new \DateTime())->format('Y-m-d\TH:i:s.uP')
        );
    }

    /**
     * @param Header $requestHeader
     * @return Header
     * @throws \Exception
     */
    private function createHeaderResponse(Header $requestHeader): Header
    {
        return new Header(
            sprintf('%s_%s', strtok($requestHeader->getType(), '_'), 'response'),
            $requestHeader->getRecipient(),
            $requestHeader->getSender(),
            $requestHeader->getReference(),
            (new \DateTime())->format('Y-m-d\TH:i:s.uP')
        );
    }
}
