<?php

namespace App\Parser;

class XmlParser implements ParserInterface
{
    /**
     * @param string $data
     * @return \SimpleXMLElement
     */
    public function parse(string $data)
    {
        return simplexml_load_string($data);
    }
}
