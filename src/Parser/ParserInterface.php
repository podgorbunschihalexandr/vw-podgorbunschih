<?php

namespace App\Parser;

/**
 * An abstraction in order to have possibility to add new parsers for different formats(json, text)
 */
interface ParserInterface
{
    public function parse(string $data);
}
