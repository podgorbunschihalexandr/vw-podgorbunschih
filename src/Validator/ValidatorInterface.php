<?php

namespace App\Validator;

/**
 * An abstraction in order to have possibility to add new validator for different formats(json, text)
 */
interface ValidatorInterface
{
    public function isValid($data): bool;
}
