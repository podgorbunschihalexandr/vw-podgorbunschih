<?php

namespace App\Validator;

use Symfony\Component\HttpKernel\KernelInterface;

/**
 * The class is responsible for validating xml again xsd schema
 */
class XmlValidator implements ValidatorInterface
{
    /**
     * @var KernelInterface
     */
    private $appKernel;

    /**
     * @param KernelInterface $appKernel
     */
    public function __construct(KernelInterface $appKernel)
    {
        $this->appKernel = $appKernel;
    }

    /**
     * @param \SimpleXMLElement $data
     * @return bool
     */
    public function isValid($data): bool
    {
        if (!$data instanceof \SimpleXMLElement) {
            // Log exception in monolog, sentry
            return false;
        }

        try {
            $domDocument = new \DOMDocument();
            $domDocument->loadXML($data->asXML());
            libxml_use_internal_errors(true);

            $path = $this->getXsdPath((string)$data->header->type);
            return $domDocument->schemaValidate($path);
        } catch (\Exception $exception) {
            // Log exception in monolog, sentry

            return false;
        }
    }

    /**
     * @param string $type
     * @return string
     */
    private function getXsdPath($type): string
    {
        return sprintf('%s/data/xsds/%s.xsd', $this->appKernel->getProjectDir(), $type);
    }
}
