<?php

namespace App\Model;

class Header
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $sender;

    /**
     * @var string
     */
    protected $recipient;

    /**
     * @var string
     */
    protected $reference;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @param string $type
     * @param string $sender
     * @param string $recipient
     * @param string $reference
     * @param string $timestamp
     */
    public function __construct(
        string $type,
        string $sender,
        string $recipient,
        string $reference,
        string $timestamp
    )
    {
        $this->type = $type;
        $this->sender = $sender;
        $this->recipient = $recipient;
        $this->reference = $reference;
        $this->createdAt = \DateTime::createFromFormat('Y-m-d\TH:i:s.uP', $timestamp);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getSender(): string
    {
        return $this->sender;
    }

    /**
     * @return string
     */
    public function getRecipient(): string
    {
        return $this->recipient;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }
}
