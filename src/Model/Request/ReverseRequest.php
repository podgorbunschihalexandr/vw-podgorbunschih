<?php

namespace App\Model\Request;

use App\Model\Header;
use App\Model\Message;
use App\Model\Request\Body\ReverseRequestBody;

class ReverseRequest extends Message
{
    /**
     * @var ReverseRequestBody
     */
    private $body;

    /**
     * @param Header $header
     * @param ReverseRequestBody $body
     */
    public function __construct(Header $header, ReverseRequestBody $body)
    {
        parent::__construct($header);
        $this->body = $body;
    }

    /**
     * @return ReverseRequestBody
     */
    public function getBody(): ReverseRequestBody
    {
        return $this->body;
    }
}
