<?php

namespace App\Model\Request\Body;

use App\Model\Body;

class ReverseRequestBody extends Body
{
    /**
     * @var string
     */
    private $string;

    /**
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->string = $string;
    }

    /**
     * @return string
     */
    public function getString(): string
    {
        return $this->string;
    }
}
