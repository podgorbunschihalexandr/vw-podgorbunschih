<?php

namespace App\Model\Request;

use App\Model\Header;
use App\Model\Message;
use App\Model\Request\Body\PingRequestBody;

class PingRequest extends Message
{
    /**
     * @var PingRequestBody
     */
    private $body;

    /**
     * @param Header $header
     * @param PingRequestBody $body
     */
    public function __construct(Header $header, PingRequestBody $body)
    {
        parent::__construct($header);
        $this->body = $body;
    }

    /**
     * @return PingRequestBody
     */
    public function getBody(): PingRequestBody
    {
        return $this->body;
    }
}
