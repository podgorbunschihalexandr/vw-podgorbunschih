<?php

namespace App\Model;

class Message
{
    /**
     * @var Header
     */
    protected $header;

    /**
     * @param Header $header
     */
    public function __construct(Header $header)
    {
        $this->header = $header;
    }

    /**
     * @return Header
     */
    public function getHeader(): Header
    {
        return $this->header;
    }

}
