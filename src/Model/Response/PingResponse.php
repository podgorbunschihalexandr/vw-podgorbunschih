<?php

namespace App\Model\Response;

use App\Model\Header;
use App\Model\Message;
use App\Model\Response\Body\PingResponseBody;

class PingResponse extends Message
{
    /**
     * @var PingResponseBody
     */
    private $body;

    /**
     * @param Header $header
     * @param PingResponseBody $body
     */
    public function __construct(Header $header, PingResponseBody $body)
    {
        parent::__construct($header);
        $this->body = $body;
    }

    /**
     * @return PingResponseBody
     */
    public function getBody(): PingResponseBody
    {
        return $this->body;
    }
}
