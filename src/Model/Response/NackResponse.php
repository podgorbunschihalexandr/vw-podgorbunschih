<?php

namespace App\Model\Response;

use App\Model\Header;
use App\Model\Message;
use App\Model\Response\Body\NackResponseBody;

class NackResponse extends Message
{
    /**
     * @var NackResponseBody
     */
    private $body;

    /**
     * @param Header $header
     * @param NackResponseBody $body
     */
    public function __construct(Header $header, NackResponseBody $body)
    {
        parent::__construct($header);
        $this->body = $body;
    }

    /**
     * @return NackResponseBody
     */
    public function getBody(): NackResponseBody
    {
        return $this->body;
    }
}
