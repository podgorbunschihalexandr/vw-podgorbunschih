<?php

namespace App\Model\Response\Body;

use App\Model\Body;

class PingResponseBody extends Body
{
    /**
     * @var string
     */
    private $echo;

    /**
     * @param string $echo
     */
    public function __construct(string $echo)
    {
        $this->echo = $echo;
    }

    /**
     * @return string
     */
    public function getEcho(): string
    {
        return $this->echo;
    }
}
