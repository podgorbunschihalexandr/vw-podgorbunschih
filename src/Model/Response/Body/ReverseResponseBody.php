<?php

namespace App\Model\Response\Body;

use App\Model\Body;

class ReverseResponseBody extends Body
{
    /**
     * @var string
     */
    private $string;

    /**
     * @var string
     */
    private $reverse;

    /**
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->string = $string;
        $this->reverse = strrev($string);
    }

    /**
     * @return string
     */
    public function getString(): string
    {
        return $this->string;
    }

    /**
     * @return string
     */
    public function getReverse(): string
    {
        return $this->reverse;
    }
}
