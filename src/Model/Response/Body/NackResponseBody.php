<?php

namespace App\Model\Response\Body;

use App\Model\Body;
use App\Model\Error;

class NackResponseBody extends Body
{
    /**
     * @var Error
     */
    private $error;

    public function __construct(Error $error)
    {
        $this->error = $error;
    }

    /**
     * @return Error
     */
    public function getError(): Error
    {
        return $this->error;
    }
}
