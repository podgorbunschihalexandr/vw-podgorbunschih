<?php

namespace App\Model\Response;

use App\Model\Header;
use App\Model\Message;
use App\Model\Response\Body\ReverseResponseBody;

class ReverseResponse extends Message
{
    /**
     * @var ReverseResponseBody
     */
    private $body;

    /**
     * @param Header $header
     * @param ReverseResponseBody $body
     */
    public function __construct(Header $header, ReverseResponseBody $body)
    {
        parent::__construct($header);
        $this->body = $body;
    }

    /**
     * @return ReverseResponseBody
     */
    public function getBody(): ReverseResponseBody
    {
        return $this->body;
    }
}
